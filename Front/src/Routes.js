import Login from './components/Login'
import FoodList from './components/FoodList'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import React, { Component } from 'react'

const Routes = props => {

    return (
    <BrowserRouter>

        <Switch>

            <Route exect path='/' exact component = {Login}></Route>
            <Route path='/Foodlist' component={FoodList}>

            </Route>

        </Switch>
    </BrowserRouter>
    )
}
export default Routes